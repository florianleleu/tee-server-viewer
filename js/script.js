var TSV = 
{
    ip : null,
    port : 8303,
    timeout : 0,
    refresh : 200,
    alive : false,
    beginTime : 0,
    init : function()
    {
        //hide errors that have occured before todo: put this elsewhere later
        $('#error').hide('slow', function()
        {

        });
    
        this.beginTime = new Date().getTime();
        this.alive = true;
        this.ip = null;
        this.port = 8303;
        this.timeout = 0;
    },
    checkAddress : function(address)
    {
        var regexp = new RegExp(/^([0-9]{1,3}\.){3}[0-9]{1,3}(:[0-9]{1,5})?$/);
        
        if(regexp.test(address))
            return true;
            
        return false;
    },
    connect : function(address)
    {
        TSV.init();
        
        if(TSV.checkAddress(address))
        {       
            address = address.split(':');
            TSV.ip = address[0];
            
            if(address[1])
                TSV.port = address[1];
            
            //setting the address in the input address
            $('#address').val(TSV.ip+':'+TSV.port);
            
            //filling out data-address of favorite and unfavorite
            $('#favorite').attr('data-address', TSV.ip+':'+TSV.port);
            $('#unfavorite').attr('data-address', TSV.ip+':'+TSV.port);
            
            //displaying the good option
            if(localStorage)
            {
                if(localStorage.getItem(TSV.ip+':'+TSV.port) === null)
                {
                    $('#favorite').css('display', 'block');
                    $('#unfavorite').css('display', 'none');
                }
                else
                {
                    $('#favorite').css('display', 'none');
                    $('#unfavorite').css('display', 'block');
                }
            }
            
            TSV.update();
        }
    },
    disconnect : function()
    {
        TSV.alive = false;
        $('#address').val('');
    },
    favorite : function(address)
    {
        if(localStorage)
        {
            if(address)
            {
                if(localStorage.getItem(address) === null)
                {
                    localStorage.setItem(address, 1);
                    $('#unfavorite').css('display', 'block');
                    $('#favorite').css('display', 'none'); 
                    TSV.menuFavorite();            
                }
            }
        }
    },
    unfavorite : function(address)
    {
        if(localStorage)
        {
            if(address)
            {
                localStorage.removeItem(address);
                $('#unfavorite').css('display', 'none');
                $('#favorite').css('display', 'block');                        
                TSV.menuFavorite();
            }
        }    
    },
    menuFavorite : function()
    {
        if(localStorage)
        {
            if(localStorage.length > 0)
            {      
                //clean the menu before to populate it
                $('#menu-favorite').empty();
            
                for(var address in localStorage)
                {
                    $('#menu-favorite').append('<li><a href="#" class="favorite-item" data-address="'+address+'">'+address+'</a></li>');
                }
                $('#menu-favorite').css('visibility', 'display');
            }
            else
                $('#menu-favorite').css('visibility', 'hidden');
        }
    },
    update : function()
    {
        if(TSV.alive)
        {
            $.ajax
            (
                {
                    type : "POST",
                    url : 'handler.php',
                    data : {'address' : TSV.ip+':'+TSV.port},
                    timeout : TSV.refresh,
                    complete : function(jqXHR, textStatus)
                    {
                        if(textStatus === "success")
                        {
                            if(jqXHR.responseText)
                            {
                                //reset to 0 number of timeout when at least one good response
                                TSV.timeout = 0;
                                
                                var data = $.parseJSON(jqXHR.responseText);
                                
                                //updating server info
                                if(data.server)
                                {
                                    var server = data.server;
                                    
                                    var maxPlayers = 0, numPlayers = 0;
                                    for(var info in server)
                                    {
                                        $('#'+info).find('span').html(server[info]);
                                    
                                        if(info === "ServerName")
                                            $('#server-title').html(server[info]);
                                        else if(info === "ServerNumPlayers")
                                            numPlayers = server[info];
                                        else if(info === "ServerMaxPlayers")
                                            maxPlayers = server[info];
                                    }

                                    var percentage = (numPlayers * 100)/maxPlayers;
                                    $('#info-players-num').html(numPlayers+'/'+maxPlayers);
                                    $('#info-players-per').attr('style', 'width:'+percentage+'%;');
                                }
                                
                                //updating player info
                                if(data.players)
                                {
                                    var players = data.players;
                                    
                                    $('#players').empty();
                                    for(var i = 0 ; i < players.length ; i++)
                                    {
                                        var player = players[i];
                                        $('#players').append('<tr><td>'+player.name+'</td><td>'+player.score+'</td></tr>');
                                    }   
                                }
                            }
                        }
                        else if(textStatus === "timeout")
                        {
                            if(TSV.timeout < 4)
                            {
                                TSV.timeout++;
                            }
                            else
                            {
                                TSV.alive = false;
                                //for fun
                                var snd = new Audio("data/sounds/error.mp3");

                                $('#error').show('slow', function()
                                {
                                    snd.play();
                                });
                                
                                //prevent from calling the setTimeout
                                return;
                            }                                      
                        }
                        
                        setTimeout("TSV.update()", TSV.refresh);
                    }
                }
            );
        }          
        
        //print time following this server
        console.log((new Date().getTime() - TSV.beginTime));
    }
};


$(document).ready(function()
{
    //setting up the menu
    TSV.menuFavorite();
    
    //init dropdown, tooltip and block autocomplete
    $('.dropdown-toggle').dropdown();
    $('#address').tooltip({'trigger':'hover', 'title': 'ex : 91.121.181.56:8305'});
    $('#address').attr("autocomplete", "off");
    
    //validating input address on the fly
    $('#address').bind('keyup change', function(event)
    {        
        $('#form-follow').find('button[type=submit]').addClass('disabled');
        
        if(TSV.checkAddress($(this).val()))
            $('#form-follow').find('button[type=submit]').removeClass('disabled');
    });
    
    //starting to follow a server from the form
    $('#form-follow').submit(function(event)
    {
        event.preventDefault();

        if(!$('#form-follow').find('button[type=submit]').hasClass('disabled'))
        {
            var address = $(this).find('#address').val();
            TSV.connect(address);
        }
    });
    
    //disconnect
    $('#disconnect').click(function(event)
    {
        event.preventDefault();
        TSV.disconnect();
    });
    
    //dealing with favorite
    $('#favorite').click(function(event)
    {
        event.preventDefault();
        TSV.favorite($(this).attr('data-address'));
    });

    $('#unfavorite').click(function(event)
    {
        event.preventDefault();
        TSV.unfavorite($(this).attr('data-address'));
    });
    
    $('.favorite-item').live('click', function(event)
    {
        event.preventDefault();
        TSV.connect($(this).attr('data-address'));
    });
    
    //error closing
    $('button.close').click(function(event)
    {
        event.preventDefault();
        var parent = $(this).parent().get(0);
        $(parent).hide('slow');
    });
});