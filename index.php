<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tee Server Viewer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <link rel="icon" type="image/png" href="img/favicon.png" />

    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"/></script>
    <script type="text/javascript" src="js/script.js"/></script>
    
    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
        body 
        {
            background: url("http://www.teeworlds.com/images/bg.png") repeat-x top left;
            background-color: #91C5FF;
        }
        
        #wrapper
        {
            margin-top : 60px;
        }
        
        div[class^="span"]
        {
            margin-top: 20px;
        }
    </style>

    <body>

        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                
                    <a class="brand" href="#">Tee Server Viewer</a>
                    <div class="nav-collapse">
                        <ul class="nav">
                            <li class="active"><a href="#">Home</a></li>
                            
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">Favorites<span class="caret"></span></a>
                                <ul class="dropdown-menu" id="menu-favorite">
  
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id="wrapper" class="container">

            <h1>Tee Server Viewer <small>follow a teeworlds server in real time</small></h1>
              
            <div class="row">
                
                <div class="span4">
                    <div class="row">
                        
                        <div class="span4">
                            <h3>What's that?</h3>
                            <div class="well">    
                                <p>Tee Server Viewer is just a website to see in real time what's going on in a Teeworlds Server.</p>
                                <p>It works using the api <a href="http://tee-stats.de/" target="_blank" alt="http://tee-stats.de/">tee-stats</a>.</p>
                                <p>Soon we'll use our own api for performance.</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="span4">
                            <h3>Follow a server</h3>
                            <div class="well" id="form-follow">
                                <form>
                                    <div class="input-prepend input-append">
                                        <span class="add-on"><i class="icon-globe"></i></span><input name="address" id="address" type="text" class="span2">
                                        <button type="submit" class="btn btn-primary disabled">Follow !</button>
                                    </div>     
                                </form>
                            </div>
                        </div>
                    </div>
                        
                    <div class="row">
                        <div class="span4">
                            <h3>About</h3>
                            <div class="well">
                                <p>Few links about the project and me ;)</p>
                                <ul class="unstyled">
                                    <li><a href="https://bitbucket.org/florianleleu/tee-server-viewer" target="_blank">The code</a></li>
                                    <li><a href="https://florianleleu.wordpress.com/" target="_blank">My blog</a></li>
                                    <li><a href="https://www.teeworlds.com" target="_blank">Teeworlds</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="wrap-info" class="span8">
                
                    <div id="error" class="alert" style="display:none;">
                        <button class="close">×</button>
                        Wether we couldn't reach this ip or the connection takes too much time !
                    </div>
                
                    <div class="btn-group pull-right">
                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-asterisk icon-white"></i> Options <span class="caret"></span></a>
                        
                        <ul class="dropdown-menu">
                            <li><a href="#" id="favorite" data-address=""><i class="icon-star"></i> <span>Favorite</span></a></li>
                            <li><a href="#" id="unfavorite" data-address=""><i class="icon-star-empty"></i> <span>Unfavorite</span></a></li>
                            <li><a href="#" id="disconnect"><i class="icon-remove"></i> <span>Disconnect</span></a></li>
                        </ul>
                    </div>
                
                    <h2 id="server-title"></h2>
                
                    <div id="wrap-info-server" class="row">
                        
                        <div id="info" class="span5">
                        
                            <ul class="unstyled">
                                <li id="ServerName"><span class="label"></span></li>
                                <li id="ServerGametype"><span class="label label-important"></span></li>
                                <li id="ServerMap"><span class="label label-success"></span></li>  
                                <li id="ServerVersion"><span class="label label-info"></span></</li>
                            </ul>
                            
                            <div id="info-players">
                                <p id="info-players-num" class="pull-right"></p>
                                <div class="progress">
                                    <div id="info-players-per" class="bar" style="width: 0%;"></div>
                                </div>
                            </div>
                        </div>
                        
                        <div id="info-map" class="span3">
                            <img src="http://placehold.it/300x150&text=no+map+to+display" alt="map of the server">
                        </div>
                    </div>
                    
                    <div id="wrap-players" class="row">
                        <div class="span8">
                            <table class="table table-striped table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th>name</th>
                                        <th>score</th>
                                    </tr>
                                </thead>
                                <tbody id="players">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>