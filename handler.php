<?php    
    if(isset($_POST['address']))
    {    
        $address = $_POST['address'];
        $address = explode(':', $address);
        $ip = $address[0];
        $port = $address[1];
    
        $url = "http://tee-stats.de/api.php?ip=".$ip."&port=".$port."&api=xml";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);
        
        if($curl)
        {
            $data = curl_exec($curl);
            curl_close($curl);
            
            if($data)
            {
                $xml = new DOMDocument();
                if(@$xml->loadXML($data))
                {
                    //init
                    $result = array('server' => array(), 'players' => array());
                    
                    //server info
                    $info = $xml->getElementsByTagName('ServerInfoMain')->item(0)->childNodes;
                    
                    foreach($info as $node)
                    {
                        //to get rid of the '\n' send by the api >_<
                        $value = trim($node->nodeValue);
                        if(!empty($value))
                            $result['server'][$node->nodeName] = $value;
                    }
                    
                    //deal with the players
                    $players = $xml->getElementsByTagName('Player');
                    
                    if($players->length > 0)
                    {
                        foreach($players as $player)
                        {
                            $result['players'][] = array
                            (
                                "name" => $player->getElementsByTagName('PlayerName')->item(0)->nodeValue, 
                                "score" => $player->getElementsByTagName('PlayerScore')->item(0)->nodeValue
                            );
                            
                            //to sort them
                            $scores [] =  $player->getElementsByTagName('PlayerScore')->item(0)->nodeValue;
                        }
                        
                        //sorting scores
                        array_multisort($scores, SORT_DESC, $result['players']);
                    }
                        
                    //sending content
                    echo json_encode($result);
                    exit;
                }
            }
        }
        
        //if fails
        echo null;
    }
?>